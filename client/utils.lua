function GetVehiclePedStandsOn(ped)
  if not IsPedOnVehicle(ped) then
    return 0
  end

  local pos = GetEntityCoords(ped)
  local ray = StartShapeTestRay(pos.x, pos.y, pos.z, pos.x, pos.y, pos.z - 2, 2)
  local _, hit, _, _, vehicle = GetShapeTestResult(ray)

  return hit and vehicle or nil
end
